package com.ggs.nofear.acty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ggs.nofear.MainActivity;
import com.ggs.nofear.R;

public class Tehnika extends AppCompatActivity {
    Button back;
    @Override
    public void onBackPressed() {
        Intent back = new Intent(Tehnika.this, MainActivity.class);
        startActivity(back);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tehnika);
        back = findViewById(R.id.back5);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(Tehnika.this, MainActivity.class);
                startActivity(back);
                finish();
            }
        });


    }
}