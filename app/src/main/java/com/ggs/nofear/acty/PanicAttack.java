package com.ggs.nofear.acty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ggs.nofear.MainActivity;
import com.ggs.nofear.R;

public class PanicAttack extends AppCompatActivity {
    Button back;
    @Override
    public void onBackPressed() {
        Intent back = new Intent(PanicAttack.this, MainActivity.class);
        startActivity(back);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panic_attack);
        back = findViewById(R.id.back1);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(PanicAttack.this, MainActivity.class);
                startActivity(back);
                finish();
            }
        });


    }
}