package com.ggs.nofear;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ggs.nofear.acty.*;

public class MainActivity extends AppCompatActivity {
CardView izbavlenie, prichini, depression, panic, narsov, tehnika;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        izbavlenie = findViewById(R.id.izbaclenie);
        prichini = findViewById(R.id.prichinisraha);
        depression = findViewById(R.id.depr);
        panic = findViewById(R.id.panic);
        narsov = findViewById(R.id.narsov);
        tehnika = findViewById(R.id.tehnika);



        izbavlenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent izb = new Intent(MainActivity.this, StopFear.class);
                startActivity(izb);
            }
        });
        prichini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pr = new Intent(MainActivity.this, PrichiniStrahov.class);
                startActivity(pr);
            }
        });


        depression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent de = new Intent(MainActivity.this, Depression.class);
                startActivity(de);
            }
        });
        panic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pa = new Intent(MainActivity.this, PanicAttack.class);
                startActivity(pa);
            }
        });
        narsov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ns = new Intent(MainActivity.this, NarSov.class);
                startActivity(ns);
            }
        });

        tehnika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent teh = new Intent(MainActivity.this, Tehnika.class);
                startActivity(teh);
            }
        });
    }
}